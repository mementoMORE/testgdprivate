var gulp = require('gulp'),
    sass = require("gulp-sass"),
    bulkSass = require('gulp-sass-bulk-import'),
    autoprefixer = require('gulp-autoprefixer'),
    server = require('gulp-server-livereload'),
    cssmin = require('gulp-cssmin'),
    concat = require('gulp-concat');

gulp.task('scss', function() {
    gulp.src('scss/app.scss')
        .pipe(bulkSass())
        .pipe(sass(
            {
                includePaths: []
            }
        ).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['>= 5%']
        }))
        // .pipe(cssmin())
        .pipe(gulp.dest('front-end/assets/css'));
});

gulp.task('scripts', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './front-end/assets/scripts/header_media.js'
    ])
        .pipe(concat('my-jquery.js'))
        .pipe(gulp.dest('./front-end/assets/scripts'));
});

gulp.task('webserver', function() {
    gulp.src('front-end')
        .pipe(server({
            host: 'localhost',
            defaultFile: 'index.html',
            livereload: true,
            directoryListing: false,
            open: true
        }));

});

gulp.task('watch', function() {
    gulp.watch('scss/**/*', ['scss']);
});

gulp.task('build', ['scss']);
gulp.task('default', ['watch', 'build']);
