let prev = document.getElementById("slider-prev");
let next = document.getElementById("slider-next");
let slideIndex = 0;

prev.addEventListener("mousedown", prevSlide);
next.addEventListener("mousedown", nextSlide);

let slides = document.getElementsByClassName("e-slide-wr");
let firstSlide = slides[0];
let firstSlideClone = slides[0].cloneNode(true);

firstSlide.parentNode.insertBefore(firstSlideClone, firstSlide.parentNode.lastChild);

//Call
showSlides(slideIndex);

function prevSlide() {
    moveSlide (-1);
    console.log('min');
}

function nextSlide() {
    moveSlide (1);
    console.log('max');
}

function moveSlide (n) {
    showSlides(slideIndex += n);
}

function showSlides(n) {
    let i;
    if (n > slides.length-2) {
        slideIndex = 0;
    }
    if (n < 0) {
        slideIndex = slides.length-2;
    }
    for (i = 0; i < slides.length; i++){
        slides[i].style.display = "none";
    }

    slides[slideIndex].style.display = "block";
    slides[slideIndex + 1].style.display = "block";
}